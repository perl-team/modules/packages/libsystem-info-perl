Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/System-Info
Upstream-Contact: H.Merijn Brand <hmbrand@cpan.org>
Upstream-Name: System-Info

Files: *
Copyright: 2016-2025, H.Merijn Brand <hmbrand@cpan.org>
           2016-2025, Abe Timmerman <abeltje@cpan.org>
License: Artistic or GPL-1+
Comment: The files in 't/etc/netbsd-*/release' were copied from a
 NetBSD system and used in the package test suite.  Those files
 contain information about those releases, including the release
 copyright.  As such, the copyright notices in those files are not
 copyright claims over those file contents.  See clarification from
 NetBSD project at our request at
 https://mail-index.netbsd.org/netbsd-users/2017/07/13/msg019836.html
 Similarly, for the files in 't/etc/solaris-11.4/release' as well.

Files: debian/*
Copyright: 2017, Carnë Draug <carandraug+dev@gmail.com>
           2024, Nick Morrott <nickm@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
